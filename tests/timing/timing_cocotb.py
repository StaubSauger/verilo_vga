from math import isclose

import cocotb
from cocotb.clock import Clock
from cocotb.regression import TestFactory
from cocotb.triggers import RisingEdge
from cocotb.utils import get_sim_time
from cocotb.result import TestError, TestFailure
from collections import namedtuple


@cocotb.coroutine
async def check_hsync(dut,
                      pixel_n=640,
                      front_porch=16,
                      sync=96,
                      back_porch=48):
    pixel = 0
    while True:
        if pixel < pixel_n:
            if dut.hsync != 1:
                raise TestFailure("HSync low during Pixel Time: " + str(pixel))
            if dut.h_pixel_address != pixel:
                raise TestFailure(
                    "Wrong Adress during H-Pixel Time expected: " +
                    str(pixel) + " but got: " + str(dut.h_pixel_address))
        elif pixel < pixel_n + front_porch:
            if dut.hsync != 1:
                raise TestFailure("HSync low during Front Porch: " +
                                  str(pixel))
            if dut.h_pixel_address != 0:
                raise TestFailure(
                    "Wrong Adress during H-Front Porch expected: 0 but got: " +
                    str(dut.h_pixel_address))
        elif pixel < pixel_n + front_porch + sync:
            if dut.hsync != 0:
                raise TestFailure("HSync high during Sync Pulse: " +
                                  str(pixel))
            if dut.h_pixel_address != 0:
                raise TestFailure(
                    "Wrong Adress during H-Sync expected: 0 but got: " +
                    str(dut.h_pixel_address))
        elif pixel < pixel_n + front_porch + sync + back_porch:
            if dut.hsync != 1:
                raise TestFailure("HSync low during Back Porch: " + str(pixel))
            if dut.h_pixel_address != 0:
                raise TestFailure(
                    "Wrong Adress during H-Back Porch expected: 0 but got: " +
                    str(dut.h_pixel_address))
        await RisingEdge(dut.clk)
        pixel = (pixel + 1) % (pixel_n + front_porch + sync + back_porch)


@cocotb.coroutine
async def check_vsync(dut,
                      h_pixel=800,
                      pixel_n=480,
                      front_porch=10,
                      sync=2,
                      back_porch=33):
    pixel = 0
    counter = 0
    while True:
        if pixel < pixel_n:
            if dut.vsync != 1:
                raise TestFailure("VSync low during Pixel Time: " + str(pixel))
            if dut.v_pixel_address != pixel:
                raise TestFailure(
                    "Wrong Adress during V-Pixel Time expected: " +
                    str(pixel) + "but got: " + str(dut.v_pixel_address))

        elif pixel < pixel_n + front_porch:
            if dut.vsync != 1:
                raise TestFailure("VSync low during Front Porch: " +
                                  str(pixel))
            if dut.v_pixel_address != 0:
                raise TestFailure(
                    "Wrong Adress during V-Front Porch expected: 0 but got: " +
                    str(dut.v_pixel_address))
        elif pixel < pixel_n + front_porch + sync:
            if dut.vsync != 0:
                raise TestFailure("VSync high during H-Sync Pulse: " +
                                  str(pixel))
            if dut.v_pixel_address != 0:
                raise TestFailure(
                    "Wrong Adress during V-Sync-Pulse expected: 0 but got: " +
                    str(dut.v_pixel_address))

        elif pixel < pixel_n + front_porch + sync + back_porch:
            if dut.vsync != 1:
                raise TestFailure("VSync low during Sync Pulse: " + str(pixel))
            if dut.v_pixel_address != 0:
                raise TestFailure(
                    "Wrong Adress during V-Back Porch expected: 0 but got: " +
                    str(dut.v_pixel_address))

        await RisingEdge(dut.clk)
        counter += 1
        if counter == h_pixel:
            pixel = (pixel + 1) % (pixel_n + front_porch + sync + back_porch)
            counter = 0


@cocotb.coroutine
async def test_two_frames(dut, resolution):
    clock = cocotb.fork(Clock(dut.clk, resolution.period, units='ns').start())
    dut.nrst = 0
    for i in range(10):
        await RisingEdge(dut.clk)

    dut.nrst = 1
    await RisingEdge(dut.clk)
    start_time_ns = get_sim_time(units='ns')
    horizontal_total = resolution.h.pixel + resolution.h.front_porch + resolution.h.sync + resolution.h.back_porch

    vertical_total = resolution.v.pixel + resolution.v.front_porch + resolution.v.sync + resolution.v.back_porch

    hsync_checker = cocotb.fork(
        check_hsync(dut, resolution.h.pixel, resolution.h.front_porch,
                    resolution.h.sync, resolution.h.back_porch))
    vsync_checker = cocotb.fork(
        check_vsync(dut, horizontal_total, resolution.v.pixel,
                    resolution.v.front_porch, resolution.v.sync,
                    resolution.v.back_porch))

    frame_time = resolution.period * (horizontal_total * vertical_total) * 2
    while True:
        await RisingEdge(dut.clk)
        this_time = get_sim_time(units='ns')
        if isclose(start_time_ns + frame_time, this_time):
            hsync_checker.kill()
            vsync_checker.kill()
            clock.kill()
            break


SyncStruct = namedtuple("Sync", "pixel, front_porch, sync, back_porch")
ResolutionStruct = namedtuple("Resolution", "h, v, period")

standart_resolution_h = SyncStruct(pixel=640,
                                   front_porch=16,
                                   sync=96,
                                   back_porch=48)
standart_resolution_v = SyncStruct(pixel=480,
                                   front_porch=11,
                                   sync=2,
                                   back_porch=31)
standart_resolution = ResolutionStruct(h=standart_resolution_h,
                                       v=standart_resolution_v,
                                       period=42.0)

factory = TestFactory(test_two_frames)
factory.add_option("resolution", [standart_resolution])
factory.generate_tests()
