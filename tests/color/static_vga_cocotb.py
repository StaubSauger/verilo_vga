from math import isclose

import cocotb
from cocotb.clock import Clock
from cocotb.regression import TestFactory
from cocotb.triggers import RisingEdge
from cocotb.utils import get_sim_time
from cocotb.result import TestError, TestFailure
from collections import namedtuple

from PIL import Image


def get_image_coordinates(counter, resolution):
    horizontal_total = resolution.h.pixel + resolution.h.front_porch + resolution.h.sync + resolution.h.back_porch

    vertical_total = resolution.v.pixel + resolution.v.front_porch + resolution.v.sync + resolution.v.back_porch

    x = int(counter % horizontal_total)
    y = int((counter // horizontal_total) % vertical_total)

    if x >= resolution.h.pixel:
        x = 0
        y = y + 1
    if y >= resolution.v.pixel:
        y = 0

    return x, y


@cocotb.coroutine
async def dump_frame(dut, name, resolution):
    img = Image.new('RGB', (resolution.h.pixel, resolution.v.pixel), "black")
    pixels = img.load()
    pixels[0, 0] = (int(dut.red), int(dut.green), int(dut.blue))
    counter = 1
    while True:
        await RisingEdge(dut.clk)
        try:
            pixels[get_image_coordinates(counter,
                                         resolution)] = (int(dut.red),
                                                         int(dut.green),
                                                         int(dut.blue))

            counter += 1
        except:
            raise TestFailure("Error storing pixels, wrong resolution?")
        if get_image_coordinates(counter, resolution) == (0, 0):
            print(counter)
            print(get_image_coordinates(counter, resolution))
            break
    img.save('./dump/' + name + ".jpeg", "JPEG")


@cocotb.coroutine
async def dump_single_frame(dut, resolution):
    clock = cocotb.fork(Clock(dut.clk, resolution.period, units='ns').start())
    dut.nrst = 0
    for i in range(10):
        await RisingEdge(dut.clk)

    dut.nrst = 1
    await RisingEdge(dut.clk)
    dumper = cocotb.fork(dump_frame(dut, "Single", resolution))
    await dumper


SyncStruct = namedtuple("Sync", "pixel, front_porch, sync, back_porch")
ResolutionStruct = namedtuple("Resolution", "h, v, period")

standart_resolution_h = SyncStruct(pixel=640,
                                   front_porch=16,
                                   sync=64,
                                   back_porch=80)
standart_resolution_v = SyncStruct(pixel=480,
                                   front_porch=20,
                                   sync=4,
                                   back_porch=13)
standart_resolution = ResolutionStruct(h=standart_resolution_h,
                                       v=standart_resolution_v,
                                       period=42.0)

factory = TestFactory(dump_single_frame)
factory.add_option("resolution", [standart_resolution])
factory.generate_tests()
