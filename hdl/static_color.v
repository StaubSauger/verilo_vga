module static_color (/*AUTOARG*/
   // Outputs
   red, green, blue,
   // Inputs
   clk, nrst, h_pixel_address, v_pixel_address
   ) ;

   parameter  H_PIXEL = 640;
   parameter V_PIXEL = 480;

   localparam H_PIXEL_WIDTH = $clog2(H_PIXEL);
   localparam V_PIXEL_WIDTH = $clog2(V_PIXEL);

   input wire clk;
   input wire nrst;

   output reg [7:0] red;
   output wire [7:0] green;
   output reg [7:0] blue;

   input wire [H_PIXEL_WIDTH-1:0] h_pixel_address;
   input wire [V_PIXEL_WIDTH-1:0] v_pixel_address;


   always @ (/*AS*/h_pixel_address or v_pixel_address) begin
      if (h_pixel_address >= 320)
        red = 255;
      else
        red = 0;

      if(v_pixel_address >= 240)
        blue = 255;
      else
        blue = 0;
   end
      
   assign green = 20;

endmodule // _color
