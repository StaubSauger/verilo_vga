`include "VGA.v"

module static_vga (/*AUTOARG*/
   // Outputs
   hsync, vsync, red, green, blue,
   // Inputs
   clk, nrst
   );

import VGA::*;

   input wire clk;
   input wire nrst;

   output wire hsync;
   output wire vsync;

   output wire [7:0] red;
   output wire [7:0] green;
   output wire [7:0] blue;

   wire [H_PIXEL_WIDTH-1:0] h_pixel_address;
   wire [V_PIXEL_WIDTH-1:0] v_pixel_address;
   wire                     new_frame;

   timing timing_inst (/*AUTOINST*/
                       // Outputs
                       .hsync           (hsync),
                       .vsync           (vsync),
                       .new_frame       (new_frame),
                       .h_pixel_address (h_pixel_address[H_PIXEL_WIDTH-1:0]),
                       .v_pixel_address (v_pixel_address[V_PIXEL_WIDTH-1:0]),
                       // Inputs
                       .clk             (clk),
                       .nrst            (nrst));


   static_color video_source(/*AUTOINST*/
                             // Outputs
                             .red               (red[7:0]),
                             .green             (green[7:0]),
                             .blue              (blue[7:0]),
                             // Inputs
                             .clk               (clk),
                             .nrst              (nrst),
                             .h_pixel_address   (h_pixel_address[H_PIXEL_WIDTH-1:0]),
                             .v_pixel_address   (v_pixel_address[V_PIXEL_WIDTH-1:0]));
endmodule: static_vga
