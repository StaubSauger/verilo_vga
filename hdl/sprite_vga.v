`include "VGA.v"

module sprite_vga (/*AUTOARG*/
   // Outputs
   hsync, vsync, red, green, blue,
   // Inputs
   clk, nrst
   );

import VGA::*;

   input wire clk;
   input wire nrst;

   output wire hsync;
   output wire vsync;

   output reg [7:0] red;
   output reg [7:0] green;
   output reg [7:0] blue;

   wire [H_PIXEL_WIDTH-1:0] h_pixel_address;
   wire [V_PIXEL_WIDTH-1:0] v_pixel_address;
   wire                     activate;

   /*AUTOWIRE*/
   // Beginning of automatic wires (for undeclared instantiated-module outputs)
   wire                 new_frame;              // From timing_inst of timing.v
   // End of automatics

   wire [7:0]           red_bg, blue_bg, green_bg;
   wire [7:0]           red_sprite, green_sprite, blue_sprite;
   wire                 active;
   
           
   

   timing timing_inst (/*AUTOINST*/
                       // Outputs
                       .hsync           (hsync),
                       .vsync           (vsync),
                       .new_frame       (new_frame),
                       .h_pixel_address (h_pixel_address[H_PIXEL_WIDTH-1:0]),
                       .v_pixel_address (v_pixel_address[V_PIXEL_WIDTH-1:0]),
                       // Inputs
                       .clk             (clk),
                       .nrst            (nrst));


   static_color video_source(
                             // Outputs
                             .red               (red_bg[7:0]),
                             .green             (green_bg[7:0]),
                             .blue              (blue_bg[7:0]),
                             // Inputs
                             .clk               (clk),
                             .nrst              (nrst),
                             .h_pixel_address   (h_pixel_address[H_PIXEL_WIDTH-1:0]),
                             .v_pixel_address   (v_pixel_address[V_PIXEL_WIDTH-1:0]));

   sprite sprite_inst(
                      // Outputs
                      .red              (red_sprite[7:0]),
                      .green            (green_sprite[7:0]),
                      .blue             (blue_sprite[7:0]),
                      .active           (active),
                      // Inputs
                      .clk              (clk),
                      .nrst             (nrst),
                      .h_pixel_address  (h_pixel_address[H_PIXEL_WIDTH-1:0]),
                      .v_pixel_address  (v_pixel_address[V_PIXEL_WIDTH-1:0]),
                      .x_pos            (20),
                      .y_pos            (30));

   always @(/*AS*/active or blue_bg or blue_sprite or green_bg or green_sprite
            or red_bg or red_sprite) begin
      if (active)
         {red, green,blue} = {red_sprite, green_sprite, blue_sprite};
      else
        {red, green, blue} = {red_bg, green_bg, blue_bg};
   end
endmodule: sprite_vga
