`include "VGA.v"

module sprite (/*AUTOARG*/
               // Outputs
               red, green, blue, active,
               // Inputs
               clk, nrst, h_pixel_address, v_pixel_address, x_pos, y_pos
               );

   import VGA::*;

   input wire clk;
   input wire nrst;

   output reg [7:0] red;
   output reg [7:0] green;
   output reg [7:0] blue;
   output reg       active;

   input wire [H_PIXEL_WIDTH-1:0] h_pixel_address;
   input wire [V_PIXEL_WIDTH-1:0] v_pixel_address;

   input wire [H_PIXEL_WIDTH-1:0] x_pos;
   input wire [V_PIXEL_WIDTH-1:0] y_pos;

   parameter x_length = 64,
     y_length = 64;

   reg  [H_PIXEL_WIDTH-1:0]                          x_index, y_index;

   logic [23:0]                   sprite_map [15:0][15:0];
   logic                          enable_map [15:0][15:0];

   initial begin
      $readmemh("mario_sprite/A.mem", enable_map, 0, 255);
      $readmemh("mario_sprite/RGB.mem", sprite_map, 0, 255);
   end

   always @ (posedge clk) begin
      if ((h_pixel_address >= x_pos) && (h_pixel_address < x_pos + x_length) && (v_pixel_address >= y_pos) &&(v_pixel_address < y_pos + y_length))  begin
         x_index = (h_pixel_address - x_pos) >> 2;
         y_index = (v_pixel_address - y_pos) >> 2;
         active <= enable_map[x_index][y_index];
         red <= sprite_map[x_index][y_index][23:16];
         green <= sprite_map[x_index][y_index][15:8];
         blue <= sprite_map[x_index][y_index][7:0];
      end else begin
         active <= 0;
         red <= 0;
         green <= 0;
         blue <= 0;
      end
   end
endmodule: sprite
