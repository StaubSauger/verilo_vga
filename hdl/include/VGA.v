`ifndef VGA_h
`define VGA_h

package VGA;

   parameter  H_PIXEL = 640;
   parameter V_PIXEL = 480;

   parameter H_PIXEL_WIDTH = $clog2(H_PIXEL);
   parameter V_PIXEL_WIDTH = $clog2(V_PIXEL);

   parameter  H_FRONT_PORCH = 16,
     H_SYNC_PULSE = 96,
     H_BACK_PORCH = 48;

   parameter V_FRONT_PORCH = 11,
     V_SYNC_PULSE = 2,
     V_BACK_PORCH = 31;

endpackage
`endif
