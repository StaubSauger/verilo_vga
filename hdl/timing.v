`include "VGA.v"
module timing (/*AUTOARG*/
   // Outputs
   hsync, vsync, new_frame, h_pixel_address, v_pixel_address,
   // Inputs
   clk, nrst
   );

import VGA::*;

   localparam H_TIMER_LENGTH = H_PIXEL + H_FRONT_PORCH + H_SYNC_PULSE + H_BACK_PORCH,
     V_TIMER_LENGTH = V_PIXEL + V_FRONT_PORCH + V_SYNC_PULSE + V_BACK_PORCH;

   localparam H_TIMER_WIDTH = $clog2(H_TIMER_LENGTH),
     V_TIMER_WIDTH = $clog2(V_TIMER_LENGTH);

   input wire  clk;
   input wire  nrst;

   output reg  hsync;
   output reg  vsync;
   output reg  new_frame;

   output reg [H_PIXEL_WIDTH - 1:0] h_pixel_address ;
   output reg [V_PIXEL_WIDTH - 1:0] v_pixel_address ;

   reg [H_TIMER_WIDTH - 1:0]        h_timer ;
   reg [V_TIMER_WIDTH - 1:0]        v_timer;

   // Timing for V and H
   always @ (posedge clk) begin
      if (nrst == 0) begin
         /*AUTORESET*/
         // Beginning of autoreset for uninitialized flops
         h_timer <= {H_TIMER_WIDTH{1'b0}};
         v_timer <= {V_TIMER_WIDTH{1'b0}};
         // End of automatics
      end else if (h_timer < H_TIMER_LENGTH - 1) begin
         h_timer <= h_timer + 1;
      end else begin
         h_timer <= 0;
         if (v_timer < V_TIMER_LENGTH - 1)
           v_timer <= v_timer + 1;
         else
           v_timer <= 0;
      end
   end

   // H Aligment and Sync Pulses
   always @ (/*AS*/h_timer) begin
      if (h_timer < H_PIXEL) begin
         hsync = 1;
         h_pixel_address = h_timer[H_PIXEL_WIDTH - 1:0];
      end else if (h_timer < H_PIXEL + H_FRONT_PORCH) begin
         hsync = 1;
         h_pixel_address = 0;
      end else if (h_timer < H_PIXEL + H_FRONT_PORCH + H_SYNC_PULSE) begin
         hsync = 0;
      end else
        hsync = 1;
   end

   // V Aligment and Sync Pulses
   always @ (/*AS*/v_timer) begin
      if (v_timer < V_PIXEL) begin
         vsync = 1;
         v_pixel_address = v_timer[V_PIXEL_WIDTH - 1:0];
      end else if (v_timer < V_PIXEL + V_FRONT_PORCH) begin
         vsync = 1;
         v_pixel_address = 0;
      end else if (v_timer < V_PIXEL + V_FRONT_PORCH + V_SYNC_PULSE)
        vsync = 0;
      else begin
         vsync = 1;
      end
   end // always @ (...

   always @(/*AS*/hsync or vsync) begin
      if (!vsync && !hsync)
        new_frame = 1;
      else
        new_frame = 0;
   end
endmodule: timing
