from PIL import Image

img = Image.open("target.png")
img_check = Image.new('RGBA', (16, 16))
pixels_check = img_check.load()

pixels = img.load()

outRGB = open("target_RGB.mem", "w")
outA = open("target_A.mem", "w")

for i in range(img.size[0] // 16):
    for j in range(img.size[1] // 16):
        pixels_check[i, j] = pixels[i * 16, j * 16]
        red = pixels[i * 16, j * 16][0] * 0x10000
        green = pixels[i * 16, j * 16][1] * 0x100
        blue = pixels[i * 16, j * 16][2]
        hex_pixel = red + blue + green
        if pixels[i * 16, j * 16][3] > 0:
            alpha = 1
        else:
            alpha = 0
        outRGB.write(hex(hex_pixel)[2:] + " ")
        outA.write(str(alpha) + " ")
    outRGB.write("\n")
    outA.write("\n")

img_check.save("target_check.png")
